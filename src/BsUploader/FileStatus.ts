export enum FileStatus {
    Pending = 0,
    Uploading = 1,
    Completed = 2,
    Failed = 3,
    Canceled = 4
}