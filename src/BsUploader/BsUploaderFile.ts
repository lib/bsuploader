import { FileStatus } from "./FileStatus";

export class BsUploaderFile {
    private _data: File;
    private _status: FileStatus;
    private _id: string;
    private _cancelTokenSource: any;

    public FileStatus: { [key: string]: number };

    constructor(file: File) {
        this.FileStatus = {
            Pending: 0,
            Uploading: 1,
            Completed: 2,
            Failed: 3,
            Canceled: 4
        };

        this._data = file;
        this._status = FileStatus.Pending;
        this._id = Math.random().toString(36).substring(2);
        this._cancelTokenSource = null;
    }

    public get status(): FileStatus {
        return this._status;
    }

    public get id(): string {
        return this._id;
    }

    public get data(): File {
        return this._data;
    }

    public get filename(): string {
        return this._data.name;
    }

    public set status(status: FileStatus) {
        this._status = status;
    }

    public get canUpload(): boolean {
        return (
            this._status === FileStatus.Pending ||
            this._status === FileStatus.Failed
        );
    }

    public get cancelTokenSource(): any {
        return this._cancelTokenSource;
    }

    public set cancelTokenSource(source: any) {
        this._cancelTokenSource = source;
    }

}
