/*
 * BsUploader - Pure javascript uploader class
 * https://git.aae.wisc.edu/lib/bsuploader/
 * 
 * Based on Daniel Morale's dmUploader jQuery widget
 * https://github.com/danielm/uploader
 *
 * Copyright Eric Dieckman <eric.dieckman@wisc.edu>
 * Released under the MIT license.
 * https://github.com/ewdieckman/bsuploader/blob/master/LICENSE.txt
 *
 * @preserve
 */

import { EventDispatcher } from '../EventDispatcher';
import { BsUploaderFile } from './BsUploaderFile';

import type { FileEventArgs, UploadErrorEventArgs, UploadProgressEventArgs, UploadSuccessEventArgs } from './types'

import axios from 'axios'
import type { AxiosError, AxiosProgressEvent } from 'axios'

import { FileStatus } from './FileStatus';

export interface BsUploaderOptions {
    autoUpload: boolean;
    useQueue: boolean;
    multiple: boolean;
    endpoint: string;
    method: string;
    extraData: Record<string, any>;
    headers: Record<string, string>;
    responseType: string;
    fieldName: string;
    maxFileSize: number;
    allowedTypes: string;
    extensionFilter: string[] | null;
    limitFileNumber: number;
    onNewFile: (id: string, file: File) => boolean;
}

class BsUploaderEventDispatcher<T> extends EventDispatcher<BsUploader, T> {

}

class BsUploaderNoArgEventDispatcher extends BsUploaderEventDispatcher<any> {

}

/**
 * @class BsUploader
 * */
export default class BsUploader {
    private _options: BsUploaderOptions;
    private queue: BsUploaderFile[];
    private queuePositionIndex: number;
    private queueRunning: boolean;
    private activeFiles: number;

    // event declarations
    /**
     * Event reporting that a file has been added to the queue
     * @event BsUploader#onAddFileToQueueEvent
     * @property {BsUploaderFile} file - Uploader file
     * */
    public onAddFileToQueueEvent: BsUploaderEventDispatcher<FileEventArgs>;
    
    /**
    * Event fired right before a single file's upload is about to begin
    * @event BsUploader#onBeforeUploadEvent
    * @property {BsUploaderFile} file - Uploader file
    * */
    public onBeforeUploadEvent: BsUploaderNoArgEventDispatcher;

    /**
     * Event reporting that a file has been accepted into the system.  It has passed all validations and will
     * be queued or uploaded immediately, depending on configuration
     * This should be used when locally tracking a file's existence in BsUploader
     * @event BsUploader#onFileAcceptedEvent
     * @property {BsUploaderFile} file - Uploader file
     * */
    public onFileAcceptedEvent: BsUploaderEventDispatcher<FileEventArgs>;

    /**
    * Event fired when a file fails validation due to its extension
    * @event BsUploader#onFileExtensionErrorEvent
    * @property {BsUploaderFile} file - Uploader file
    * */
    public onFileExtensionErrorEvent: BsUploaderEventDispatcher<FileEventArgs>;

    /**
    * Event fired when trying to add a file to the queue, but the queue has reached the file number limit
    * @event BsUploader#onFileNumberLimitErrorEvent
    * @property {BsUploaderFile} file - Uploader file
    * */
    public onFileNumberLimitErrorEvent: BsUploaderEventDispatcher<FileEventArgs>;

    /**
    * Event fired when a file fails validation due to its size
    * @event BsUploader#onFileSizeErrorEvent
    * @property {BsUploaderFile} file - Uploader file
    * */
    public onFileSizeErrorEvent: BsUploaderEventDispatcher<FileEventArgs>;

    /**
    * Event fired when a file fails validation due to its file type
    * @event BsUploader#onFileTypeErrorEvent
    * @property {BsUploaderFile} file - Uploader file
    * */
    public onFileTypeErrorEvent: BsUploaderEventDispatcher<FileEventArgs>;

    /**
    * Event fired when initialization of the BsUploader class is complete
    * @event BsUploader#onInitEvent
    * */
    public onInitEvent: BsUploaderNoArgEventDispatcher;

    /**
    * Event fired when the entire upload queue has been processed and all uploads are complete
    * @event BsUploader#onQueueCompleteEvent
    * */    
    public onQueueCompleteEvent: BsUploaderNoArgEventDispatcher;

    /**
     * Event reporting that a file has been removed from the queue
     * @event BsUploader#onRemoveFileFromQueueEvent
     * @property {BsUploaderFile} file - Uploader file
     * */
    public onRemoveFileFromQueueEvent : BsUploaderEventDispatcher<FileEventArgs>;

    /**
    * Event fired when an upload has been cancelled
    * @event BsUploader#onUploadCanceledEvent
    * @property {BsUploaderFile} file - Uploader file
    * */
    public onUploadCanceledEvent : BsUploaderEventDispatcher<FileEventArgs>;
    
    /**
    * Event fired when an upload has been completed, regardless of outcome
    * @event BsUploader#onUploadCompleteEvent
    * @property {BsUploaderFile} file - Uploader file
    * */
    public onUploadCompleteEvent : BsUploaderEventDispatcher<FileEventArgs>;

    /**
    * Event fired when an upload encounters an error during the request
    * @event BsUploader#onUploadErrorEvent
    * @property {BsUploaderFile} file - Uploader file
    * @property {Object} error - axios error object returned from the request exception
    * */    
    public onUploadErrorEvent : BsUploaderEventDispatcher<UploadErrorEventArgs>;

    /**
    * Event fired periodically during a request to indicate progress
    * @event BsUploader#onUploadProgressEvent
    * @property {BsUploaderFile} file - Uploader file
    * @property {Number} percent - Percentage complete of the upload request
    * */
    public onUploadProgressEvent : BsUploaderEventDispatcher<UploadProgressEventArgs>;;

    /**
    * Event fired when an upload has successfuly completed
    * @event BsUploader#onUploadSuccessEvent
    * @property {BsUploaderFile} file - Uploader file
    * @property {Object} data - data returned from the server following the upload
    * */
    public onUploadSuccessEvent : BsUploaderEventDispatcher<UploadSuccessEventArgs>;

    constructor(options: Partial<BsUploaderOptions>) {
        const defaults: BsUploaderOptions = {
            autoUpload: true,
            useQueue: true,
            multiple: true,
            endpoint: document.URL,
            method: "POST",
            extraData: {},
            headers: {},
            responseType: 'json',
            fieldName: 'data',
            maxFileSize: 0,
            allowedTypes: "*",
            extensionFilter: null,
            limitFileNumber: 0,             // unlimited
            onNewFile: () => true           // params: id, file - for doing external validation of a file
        };

        this._options = { ...defaults, ...options };

        this.activeFiles = 0;
        this.queueRunning = false;
        this.queuePositionIndex = 0;
        this.queue = new Array<BsUploaderFile>;

        this.onAddFileToQueueEvent = new BsUploaderEventDispatcher<FileEventArgs>(this);
        this.onBeforeUploadEvent = new BsUploaderNoArgEventDispatcher(this);
        this.onFileAcceptedEvent = new BsUploaderEventDispatcher<FileEventArgs>(this);
        this.onFileExtensionErrorEvent = new BsUploaderEventDispatcher<FileEventArgs>(this);
        this.onFileNumberLimitErrorEvent = new BsUploaderNoArgEventDispatcher(this);
        this.onFileSizeErrorEvent = new BsUploaderEventDispatcher<FileEventArgs>(this);
        this.onFileTypeErrorEvent = new BsUploaderEventDispatcher<FileEventArgs>(this);
        this.onInitEvent = new BsUploaderNoArgEventDispatcher(this);
        this.onQueueCompleteEvent = new BsUploaderNoArgEventDispatcher(this);
        this.onRemoveFileFromQueueEvent = new BsUploaderEventDispatcher<FileEventArgs>(this);
        this.onUploadCanceledEvent = new BsUploaderEventDispatcher<FileEventArgs>(this);
        this.onUploadCompleteEvent = new BsUploaderEventDispatcher<FileEventArgs>(this);
        this.onUploadErrorEvent = new BsUploaderEventDispatcher<UploadErrorEventArgs>(this);
        this.onUploadProgressEvent = new BsUploaderEventDispatcher<UploadProgressEventArgs>(this);
        this.onUploadSuccessEvent = new BsUploaderEventDispatcher<UploadSuccessEventArgs>(this);

        this.init();
    }

    /**
     * Add the File(s) to the queue
     * @param {FileList} files
     * @fires BsUploader#onAddFileToQueueEvent
     */
    addFiles(files: FileList): void {
        
        let numberOfFilesAdded: number = 0;

        for (let i: number = 0; i < files.length; i++) {
            // check first if there is a limit on the maximum number of files          
            if (this._options.limitFileNumber > 0 && this.queue.length >= this._options.limitFileNumber) {
                // have we already reached it? - if yes, stop processing
                this.onFileNumberLimitErrorEvent.notify();
                break;
            }

            const bsUploaderFile: BsUploaderFile = new BsUploaderFile(files[i]);
            const validationResult: boolean = this.validateFile(bsUploaderFile);

            if (!validationResult) {
                // not valid - skip this loop
                continue;
            }

            // if the callback returns false, the file will not be process
            const canContinue: boolean = this._options.onNewFile(bsUploaderFile.id, bsUploaderFile.data);
            if (canContinue === false) {
                continue;
            }

            // file has "passed" all tests - it will be admitted to the queue (or immediately uploaded)
            // notify listeners
            this.onFileAcceptedEvent.notify({ file: bsUploaderFile });

            // if it's automatic uploading, and not a file queue: start the upload                    
            if (this._options.autoUpload && !this._options.useQueue) {
                this.uploadFile(bsUploaderFile);
            }

            this.queue.push(bsUploaderFile);
            this.onAddFileToQueueEvent.notify({ file: bsUploaderFile });
            numberOfFilesAdded++;
        }

        // no files were added        
        if (numberOfFilesAdded === 0) {
            return;
        }

        if (this._options.autoUpload && this._options.useQueue && !this.queueRunning) {
            this.processQueue();
        }
    }

    /**
     * Stops the queue and cancels all file uploads
     * @fires BsUploader#onQueueCompleteEvent
     * */
    cancelAll(): void {
        const queueWasRunning: boolean = this.queueRunning;
        this.queueRunning = false;

        // cancel all
        for (let i: number = 0; i < this.queue.length; i++) {
            this.cancelFile(this.queue[i]);
        }

        if (queueWasRunning) {
            this.onQueueCompleteEvent.notify();
        }
    }


    /**
     * Cancels the upload of a single file
     * @param {BsUploaderFile} file
     * @param {Boolean} abort - flag to track if the function was called directly or by cancelAll()
     * @fires BsUploader#onUploadCanceledEvent
     */
    cancelFile(file: BsUploaderFile, abort: boolean = false): void {
        // CHECK THIS AGAINST ORIGINAL JAVASCRIPT SOURCE
        // The abort flag is to track if we are calling this function directly (using the cancel Method, by id)
        // or the call comes from the 'global' method aka cancelAll.
        // This means that we don't want to trigger the cancel event on file that isn't uploading, UNLESS directly doing it

        const status: number = file.status;
        if (status === FileStatus.Uploading || (abort && status === FileStatus.Pending)) {
            file.status = FileStatus.Canceled;
        } else {
            return;
        }

        this.onUploadCanceledEvent.notify({ file });

        if (status === FileStatus.Uploading) {
            file.cancelTokenSource.cancel('Operation cancelled by user.');
        }
    }

    /**
     * Retrieves a file from the queue
     * @param {String} fileId - ID of the file in the queue
     * @returns {BsUploaderFile}
     */
    findById(fileId: string): BsUploaderFile | null {
        for (let i: number = 0; i < this.queue.length; i++) {
            if (this.queue[i].id === fileId) {
                return this.queue[i];
            }
        }
        return null;
    }

    /**
     * Initializes all class variables
     * @fires #onInitEvent
     * */
    init(): void {
        this.queue = [];
        this.queuePositionIndex = -1;
        this.queueRunning = false;
        this.activeFiles = 0;

        this.onInitEvent.notify();
    }

    /**
     * Method called when the specified file's upload has been completed.  This is triggered regardless if the request was successful or not
     * @param {BsUploaderFile} file
     * @fires BsUploader#onUploadCompleteEvent
     * @fires BsUploader#onQueueCompleteEvent
     */
    onUploadComplete(file: BsUploaderFile): void {
        this.activeFiles--;

        if (file.status !== FileStatus.Canceled) {
            this.onUploadCompleteEvent.notify({ file });
        }

        if (this.queueRunning) {
            this.processQueue();
        } else if (this.queue && this.activeFiles === 0) {
            this.onQueueCompleteEvent.notify();
        }
    }

    /**
     * Method called if there is an error during the specified file's upload
     * @param {BsUploaderFile} file
     * @param {Object} axiosError
     * @fires BsUploader#onUploadErrorEvent
     */
    onUploadError(file: BsUploaderFile, axiosError: AxiosError): void {

        // if the status is cancelled (by the user), don't invoke the error
        if (file.status !== FileStatus.Canceled) {
            file.status = FileStatus.Failed;

            //if (axiosError.response) {
            //    // The request was made and the server responded with a status code
            //    // that falls out of the range of 2xx
            //    console.log(axiosError.response.data);
            //    console.log(axiosError.response.status);
            //    console.log(axiosError.response.headers);
            //} else if (axiosError.request) {
            //    // The request was made but no response was received
            //    // `error.request` is an instance of XMLHttpRequest in the browser and an instance of
            //    // http.ClientRequest in node.js
            //    console.log(axiosError.request);
            //} else {
            //    // Something happened in setting up the request that triggered an Error
            //    console.log('Error', axiosError.message);
            //}
            
            this.onUploadErrorEvent.notify({
                file,
                error: axiosError
            });
        }
    }

    /**
     * Callback function from axios for reporting progress for an uploading file
     * @param {AxiosProgressEvent} progressEvent
     * @param {Number} progressEvent.loaded - integer representing the amount of work already performed by the underlying process
     * @param {Number} progressEvent.total - integer representing the total amount of work that the underlying process is in the progress of performing
     * @param {Number} progressEvent.lengthComputable - flag indicating if the resource concerned by the ProgressEvent has a length that can be calculated. If not, the ProgressEvent.total property has no significant value.
     * @param {String} id - Uploader file id
     * @param {Number} percent - Percentage that the upload is complete
     * @fires BsUploader#onUploadProgressEvent
     */
    onUploadProgress(progressEvent: AxiosProgressEvent, file: BsUploaderFile): void {
        let percent: number = 0;
        const position: number = progressEvent.loaded;
        const total: number | undefined = progressEvent.total


        if (progressEvent.event?.lengthComputable && total !== undefined) {
            percent = Math.ceil(position / total * 100);
        }

        this.onUploadProgressEvent.notify({ file, percent });
    }

    /**
     * Method called by axios when an upload is successfully completed
     * @param {BsUploaderFile} file - file that was successfully uploaded
     * @param {any} data - data returned from the upload request
     * @fires BsUploader#onUploadSuccessEvent
     */
    onUploadSuccess(file: BsUploaderFile, data: any): void {
        file.status = FileStatus.Completed;
        this.onUploadSuccessEvent.notify({ file, data });
    }

    /**
     * Processes the pending file queue.
     * @fires BsUploader#onQueueCompleteEvent
     * */
    processQueue(): void {
        this.queuePositionIndex++;

        if (this.queuePositionIndex >= this.queue.length) {
            if (this.activeFiles === 0) {
                this.onQueueCompleteEvent.notify();
            }

            // wait until new files are dropped
            this.queuePositionIndex = this.queue.length - 1;
            this.queueRunning = false;
            return;
        }

        this.queueRunning = true;

        // start next file
        this.uploadFile(this.queue[this.queuePositionIndex]);
    }

    /**
     * Removes the specified file from the queu
     * @param {String} fileId ID of the file in the queue
     */
    removeFile(fileId: string): void {
        const queueIndex: number | null = this.queue.findIndex(file => file.id === fileId);
        if (queueIndex !== null) {
            const file = this.findById(fileId)!;
            this.queue.splice(queueIndex, 1);
            this.onRemoveFileFromQueueEvent.notify({ file });
        }
    }

    /**
     * Stops the queue, resets it, and then starts the process again
     * */
    restartQueue(): void {
        this.queuePositionIndex = -1;
        this.queueRunning = false;
        this.processQueue();
    }

    /**
     * Resets the class.  Stops all uploads, removes all riles, resets the queue
     * */
    reset(): void {
        this.cancelAll();
        this.queue = new Array<BsUploaderFile>();
        this.queuePositionIndex = -1;
        this.activeFiles = 0;
    }

    /**
     * Method to start the uploading.
     * If no id specified, all pending uploads are started
     * @param {String} [fileId] File ID of the file to start upload
     * @param {Object} [extraData] Extra data to be sent with the request.  Is merge with extraData inject in the constructor
     */
    start(fileId: string | null = null, extraData: Record<string, any> | null = null): void {
        if (extraData != null) {
            this._options.extraData = { ...this._options.extraData, ...extraData };
        }

        console.log(this._options.extraData)
        if (this.queueRunning) {
            // do not allow to manually upload files when a queue is running
            return;
        }

        let file: BsUploaderFile | null = null;

        if (fileId != null) {
            file = this.findById(fileId);
            if (!file) {
                // file not found in the stack
                throw new Error('File not found in BsUploader');
            }
        }

        // try to start an update by ID
        if (file) {
            if (file.status === FileStatus.Canceled) {
                file.status = FileStatus.Pending;
            }
            this.uploadFile(file);
        } else {
            // no id provided
            this.startAll();
        }
    }

    /**
     * Resumes the queue if useQueue = true.  Otherwise, all uploads are started simultaneously
     * */
    startAll(): void {
        if (this._options.useQueue) {
            this.restartQueue();
        } else {
            for (let i: number = 0; i < this.queue.length; i++) {
                this.uploadFile(this.queue[i]);
            }
        }
    }

    /**
     * Method to initiate the upload of the file to an endpoint
     * @param {BsUploaderFile} file
     * @fires BsUploader#onBeforeUploadEvent
     * @returns {Promise}
     */
    uploadFile(file: BsUploaderFile): Promise<void> {
        const options = this._options;

        if (!file.canUpload) {
            if (this.queueRunning && file.status !== FileStatus.Uploading) {
                this.processQueue();
            }
            return Promise.resolve();
        }

        // form data
        const formData: FormData = new FormData();
        formData.append(options.fieldName, file.data, file.data.name);

        console.log(formData)
        // append extra form data
        const customData: Record<string, any> = options.extraData;

        for (const property in customData) {
            formData.append(property, customData[property]);
        }

        console.log(formData)
        file.status = FileStatus.Uploading;
        this.activeFiles++;

        // hook for before upload
        this.onBeforeUploadEvent.notify({ file });

        // get the Canceltoken and save it with teh file
        const CancelToken = axios.CancelToken;
        file.cancelTokenSource = CancelToken.source();

        // send to the server
        return axios({
            method: options.method,
            url: options.endpoint,
            data: formData,
            headers: options.headers,
            onUploadProgress: (progressEvent) => {
                this.onUploadProgress(progressEvent, file);
            },
            cancelToken: file.cancelTokenSource.token
        })
        .then(response => {
            this.onUploadSuccess(file, response.data);
        })
        .catch((error: AxiosError) => {
            this.onUploadError(file, error);
        })
        .finally(() => {
            this.onUploadComplete(file);
        });
    }

    /**
     * Validates a form file against configurations set for this uploader
     * @param {BsUploaderFile} file
     * @fires BsUploader#onFileSizeErrorEvent
     * @fires BsUploader#onFileTypeErrorEvent
     * @fires BsUploader#onFileExtensionErrorEvent
     * @return {Boolean} whether the file is valid or not
     */
    validateFile(file: BsUploaderFile): boolean {
        // check file size
        if (this._options.maxFileSize > 0 && file.data.size > this._options.maxFileSize) {
            this.onFileSizeErrorEvent.notify({ file });
            return false;
        }

        // check file type
        if (this._options.allowedTypes !== "*" && !file.data.type.match(this._options.allowedTypes)) {
            this.onFileTypeErrorEvent.notify({ file });
            return false;
        }

        // check file extension
        if (this._options.extensionFilter !== null) {
            const extension: string = file.data.name.toLowerCase().split('.').pop()!;
            if (!this._options.extensionFilter.includes(extension)) {
                this.onFileExtensionErrorEvent.notify({ file });
                return false;
            }
        }

        return true;
    }
}
