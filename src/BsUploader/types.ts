import type { BsUploaderFile } from './BsUploaderFile'
import { AxiosError } from 'axios';

export interface FileEventArgs {
    file: BsUploaderFile
}

export interface UploadErrorEventArgs extends FileEventArgs {
    error: AxiosError
}

export interface UploadProgressEventArgs extends FileEventArgs {
    percent: number
}

export interface UploadSuccessEventArgs extends FileEventArgs {
    data: any
}
