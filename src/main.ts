import type { App } from 'vue';
import { UploaderDropZone, UploaderQueueFile } from "./components";
import './scss/uploader.scss';

export default {
  install: (app: App) => {
    app.component('UploaderDropZone', UploaderDropZone);
    app.component('UploaderQueueFile', UploaderQueueFile);
  }
};

export { default as BsUploader } from "./BsUploader/BsUploader";
export { UploaderDropZone, UploaderQueueFile };
