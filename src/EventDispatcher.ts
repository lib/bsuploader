export class EventDispatcher<T, K> {
    private _sender: T;
    private _listeners: Array<(sender: T, args?: K) => void>;

    constructor(sender: T) {
        this._sender = sender;
        this._listeners = [];
    }

    attach(listener: (sender: T, args?: K) => void): void {
        this._listeners.push(listener);
    }

    notify(args?: K): void {
        for (let i = 0; i < this._listeners.length; i += 1) {
            this._listeners[i](this._sender, args);
        }
    }
}
