import { defineConfig } from "vite";
import { type Plugin } from 'vite';
import vue from "@vitejs/plugin-vue";
import path from "path";
import dts from "vite-plugin-dts";
import scss from 'rollup-plugin-scss'

export default defineConfig({
  plugins: [
    scss({
      fileName: 'bsuploader.css'
    }),
    vue(),
    dts({
      rollupTypes: true,        // merge all declarations into one file
      insertTypesEntry: true
    }), 
  ],
  resolve: {
    alias: {
      "@/": new URL("./src/", import.meta.url).pathname,
    },
  },

  build: {
    cssCodeSplit: true,
    target: "esnext",
    lib: {
      entry: path.resolve(__dirname, "src/index.ts"),
      name: "BsUploader",
      formats: ["es", "cjs", "umd"],
      fileName: (format) => `bsuploader.${format}.js`,
    },

    rollupOptions: {
      input: {
        main: path.resolve(__dirname, "src/main.ts")
      },
      external: ["vue"],
      output: {
        exports: "named",
        globals: {
          vue: "Vue",
        },
        dir: "dist"
      },
    },
  },
});



